package com.sber.java13spring.java13springproject.libraryproject.dto;

import lombok.*;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class AuthorDTO
      extends GenericDTO {
    private String authorFio;
    private LocalDate birthDate;
    private String description;
    private Set<Long> booksIds;
}
