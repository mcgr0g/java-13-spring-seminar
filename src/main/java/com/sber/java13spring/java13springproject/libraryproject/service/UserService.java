package com.sber.java13spring.java13springproject.libraryproject.service;

import com.sber.java13spring.java13springproject.libraryproject.dto.RoleDTO;
import com.sber.java13spring.java13springproject.libraryproject.dto.UserDTO;
import com.sber.java13spring.java13springproject.libraryproject.mapper.UserMapper;
import com.sber.java13spring.java13springproject.libraryproject.model.User;
import com.sber.java13spring.java13springproject.libraryproject.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService
      extends GenericService<User, UserDTO> {
    
    protected UserService(UserRepository userRepository,
                          UserMapper userMapper) {
        super(userRepository, userMapper);
    }
    
    @Override
    public UserDTO create(UserDTO object) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(1L);
        object.setRole(roleDTO);
        return mapper.toDto(repository.save(mapper.toEntity(object)));
    }
}
