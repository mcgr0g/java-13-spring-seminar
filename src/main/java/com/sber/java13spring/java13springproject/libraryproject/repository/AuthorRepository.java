package com.sber.java13spring.java13springproject.libraryproject.repository;

import com.sber.java13spring.java13springproject.libraryproject.model.Author;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository
      extends GenericRepository<Author> {
}
