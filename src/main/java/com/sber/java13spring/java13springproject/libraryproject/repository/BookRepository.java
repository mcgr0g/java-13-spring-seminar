package com.sber.java13spring.java13springproject.libraryproject.repository;

import com.sber.java13spring.java13springproject.libraryproject.model.Book;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository
      extends GenericRepository<Book> {
}
